<?php
namespace HIVE\HiveThmCustom\Hooks;
/**
 * This file is part of the "hive_thm_custom" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

/**
 * AutoConfiguration-Hook for RealURL
 *
 */
class RealUrlAutoConfiguration
{
    /**
     * Generates additional RealURL configuration and merges it with provided configuration
     *
     * @param       array $params Default configuration
     * @return      array Updated configuration
     */
    public function addConfig($params)
    {
        return array_merge_recursive(
            $params['config'],
            [
                'lastGenerated' => time()
            ]
        );
    }

    /**
     * @param array $configuration
     */
    public function postProcessConfiguration(array &$parameters) {
        /*
         * AutoConf is awesome.
         * It generates a config for
         * - each domain record
         * - each sys_language
         *
         * If there are multiple domains for one root page:
         *
         * see also: https://github.com/dmitryd/typo3-realurl/wiki/Language-Domains
         * see also: https://github.com/dmitryd/typo3-realurl/wiki/Notes-for-Integrators#configuring-languages
         *
         * 1) Add _DOMAINS config for each domain and language
         * 2) Add TypoScript
         *    # setup.txt
         *    config {
         *      sys_language_uid = 0
         *      language = en
         *    }
         *
         *    [globalVar = GP:L = 1]
         *      config {
         *        sys_language_uid = 1
         *        language = de
         *      }
         *    [global]
         *
         * 3) If first language is in URL (e.g. /en/)
         *    # setup.txt
         *    config {
         *      sys_language_uid = 0
         *      language = en
         *      defaultGetVars {
         *        L = 0
         *      }
         *    }
         *
         *    [globalVar = GP:L = 1]
         *      config {
         *        sys_language_uid = 1
         *        language = de
         *      }
         *    [global]
         *
         */
//        $parameters['config']['_DOMAINS'] = [
//            'encode' => [
//                [
//                    'GETvar' => 'L',
//                    'value' => '0',
//                    'urlPrepend' => 'http://development.localhost',
//                    'useConfiguration' => 'development.localhost',
//                ],
//                [
//                    'GETvar' => 'L',
//                    'value' => '1',
//                    'urlPrepend' => 'http://development.localhost.de',
//                    'useConfiguration' => 'development.localhost.de',
//                 ],
//                [
//                    'GETvar' => 'L',
//                    'value' => '2',
//                    'urlPrepend' => 'http://development.localhost.ch',
//                    'useConfiguration' => 'development.localhost.ch',
//                ],
//            ],
//            'decode' => [
//                'development.localhost.com' => [
//                    'GETvars' => [
//                        'L' => 0,
//                    ],
//                    'useConfiguration' => 'development.localhost',
//                ],
//                'development.localhost.de' => [
//                    'GETvars' => [
//                        'L' => 1,
//                    ],
//                    'useConfiguration' => 'development.localhost.de',
//                ],
//                'development.localhost.ch' => [
//                    'GETvars' => [
//                        'L' => 2,
//                    ],
//                    'useConfiguration' => 'development.localhost.ch',
//                ],
//            ],
//        ];
    }
}